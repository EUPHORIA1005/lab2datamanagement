package ru.kozlov.Lab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ru.kozlov.Lab.Service.PassengerService;
import ru.kozlov.Lab.db.DBService;
import ru.kozlov.Lab.model.Passenger;

import java.sql.Connection;

@SpringBootApplication
public class FileDatabaseApplication {

    @Autowired
    DBService dbService;

    @Autowired
    PassengerService passengerService;


    public static void main(String[] args) {
        SpringApplication.run(FileDatabaseApplication.class, args);
    }

    @Bean
    ApplicationRunner applicationRunner() {

        return args -> {
            Connection connection = dbService.connectToDb("", "postgres", "root");
            dbService.dropDBIfExists(connection, "titanic");
            dbService.createDatabase(connection, "titanic");

            Connection connection1 = dbService.connectToDb("titanic", "postgres", "root");
            dbService.createFucntions(connection1, "passengers");
            dbService.createTable(connection1, "passengers");
            dbService.createFindAll(connection1, "passengers");
            dbService.createFucntions(connection1, "passengers");

            Passenger passenger = new Passenger(1, "Rustam", 20);
            passengerService.insert(connection1, passenger);

            //dbService.createUserRO(connection, "user", "user");
            /*


            Passenger passenger = new Passenger(1, "Rustam", 20);
            passengerService.insert(connection1, passenger);*/


        };
    }
}
