package ru.kozlov.Lab.Service;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kozlov.Lab.model.Passenger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;


import java.util.List;


@Service
@AllArgsConstructor
public class PassengerService {

    private static final String DB = "src/main/resources/db.csv";


    public List<Passenger> findAll(Connection connection) {
        CallableStatement statement;
        List<Passenger> passengerArrayList = new ArrayList<>();
        try{
            statement = connection.prepareCall("SELECT * FROM findAll(?)");
            statement.setString(1, "passengers");

            ResultSet resultSet = statement.executeQuery();

            System.out.println(resultSet);

            while(resultSet.next()){
                Passenger passenger = new Passenger(resultSet.getObject(1, Integer.class), resultSet.getObject(2, String.class), resultSet.getObject(3, Integer.class));
                passengerArrayList.add(passenger);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return passengerArrayList;
    }

    public List<Passenger> findAll(Connection connection, int age) {
        CallableStatement statement;
        List<Passenger> passengerArrayList = new ArrayList<>();
        try{
            statement = connection.prepareCall("SELECT * FROM findByAge(?, ?)");
            statement.setString(1, "passengers");
            statement.setInt(2, age);

            ResultSet resultSet = statement.executeQuery();

            System.out.println(resultSet);

            while(resultSet.next()){
                Passenger passenger = new Passenger(resultSet.getObject(1, Integer.class), resultSet.getObject(2, String.class), resultSet.getObject(3, Integer.class));
                passengerArrayList.add(passenger);
            }

            statement.close();
            resultSet.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return passengerArrayList;
    }

    private Passenger findByRow(int row) {
        return null;
    }

    public void insert(Connection connection, Passenger passenger) throws SQLException {
        CallableStatement statement;
        statement = connection.prepareCall("SELECT insertTable( ?, ?, ?)");

        //statement.setString(1, "passengers");
        statement.setInt(1, passenger.getPassengerId());
        statement.setString(2, passenger.getName());
        statement.setInt(3, passenger.getAge());

        statement.executeQuery();
        System.out.println("Inserted " + passenger);
        statement.close();

    }

    public void update(Connection connection, Passenger passenger) throws SQLException {
        CallableStatement statement;
        statement = connection.prepareCall("SELECT updateTable(?, ?, ?)");

        //statement.setString(1, "passengers");
        statement.setInt(1, passenger.getPassengerId());
        statement.setString(2, passenger.getName());
        statement.setInt(3, passenger.getAge());

        statement.executeQuery();
        System.out.println("Updated " + passenger);
        statement.close();
    }


    public void deleteByName(Connection connection, String name)  {
        CallableStatement statement;
        try {
            statement = connection.prepareCall("SELECT deleteByName(?, ?)");
            statement.setString(1, "passengers");
            statement.setString(2, name);

            statement.executeQuery();
            System.out.println("deleted " + name);
            //statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
