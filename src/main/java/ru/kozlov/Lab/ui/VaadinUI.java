package ru.kozlov.Lab.ui;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kozlov.Lab.Service.PassengerService;
import ru.kozlov.Lab.db.DBService;
import ru.kozlov.Lab.model.Passenger;

import java.sql.Connection;
import java.sql.SQLException;

@Route("")
@CssImport("./styles/shared-styles.css")
public class VaadinUI extends VerticalLayout {

    Grid<Passenger> grid = new Grid<>(Passenger.class);

    TextField filterTextAge = new TextField();

    TextField deleteAgesField = new TextField();

    Button createDB = new Button();

    Button refresh = new Button();

    HorizontalLayout buttonsLayout = new HorizontalLayout();

    TextField login = new TextField();
    TextField password = new TextField();
    Button loginButton = new Button();


    private PassengerForm form;

    private PassengerService passengerService;

    private DBService dbService;

    private Connection connectionToPostgres;
    private Connection connection;


    public VaadinUI(PassengerService passengerService, DBService dbService) {
        this.passengerService = passengerService;
        this.dbService = dbService;
        this.connectionToPostgres = dbService.connectToDb("", "postgres", "root");
        this.connection = dbService.connectToDb("titanic", "user", "user");

        addClassName("list-view");
        setSizeFull();
        System.out.println(grid.getPropertySet().getProperties().toList());
        configureGrid();

        configureFilter();

        form = new PassengerForm();

        form.addListener(PassengerForm.SaveEvent.class, this::savePassenger);
        form.addListener(PassengerForm.DeleteEvent.class, this:: deletePassenger);
        form.addListener(PassengerForm.UpdateEvent.class, this::updatePassenger);

        //form.addListener()


        Div content = new Div(form, grid);
        content.addClassName("content");
        content.setSizeFull();

        buttonsLayout.setPadding(true);
        buttonsLayout.add(filterTextAge);
        buttonsLayout.add(deleteAgesField);
        buttonsLayout.add(createDB);
        buttonsLayout.add(refresh);
        buttonsLayout.add(loginButton);
        buttonsLayout.add(login);
        buttonsLayout.add(password);


        add(buttonsLayout, content);
        //updateDataGrid();

    }


    public void configureFilter() {

        filterTextAge.setPlaceholder("Find by age...");
        filterTextAge.setClearButtonVisible(true);
        filterTextAge.setValueChangeMode(ValueChangeMode.EAGER);
        filterTextAge.addKeyPressListener(Key.ENTER, keyPressEvent -> updateDataGridByAge());
        filterTextAge.addKeyPressListener(Key.CLEAR, keyPressEvent -> updateDataGrid());


        deleteAgesField.setPlaceholder("Delete by name...");
        deleteAgesField.setClearButtonVisible(true);
        deleteAgesField.setValueChangeMode(ValueChangeMode.EAGER);
        deleteAgesField.addKeyPressListener(Key.ENTER, keyPressEvent -> {
            deletePassengersByAge();
            updateDataGrid();
        });
        deleteAgesField.addKeyPressListener(Key.CANCEL, keyPressEvent -> updateDataGrid());
        deleteAgesField.addKeyPressListener(Key.CLEAR, keyPressEvent -> updateDataGrid());


        createDB.setText("Create database");
        createDB.addClickListener(keyPressEvent -> createDBMethod());

        refresh.setText("Refresh");
        refresh.addClickListener(keyPressEvent -> updateDataGrid());

        loginButton.setText("login as ->");
        loginButton.addClickListener(keyPressEvent -> loginAsAdmin());

        login.setPlaceholder("Login");
        password.setPlaceholder("Password");

    }

    private void loginAsAdmin(){
        this.connection = dbService.connectToDb("titanic", login.getValue(), password.getValue());
    }

    private void createDBMethod(){
        try {
            dbService.dropDBIfExists(connectionToPostgres, "titanic");
            dbService.createDatabase(connectionToPostgres, "titanic");

            dbService.createFucntions(connection, "passengers");
            dbService.createTable(connection, "passengers");
            dbService.createFindAll(connection, "passengers");
            dbService.createFucntions(connection, "passengers");

        }catch (SQLException exception){} catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



    private void configureGrid() {
        grid.addClassName("Passenger-grid");
        grid.setSizeFull();
        grid.setColumns("passengerId", "name", "age");


        grid.asSingleSelect().addValueChangeListener(event -> editPassenger(event.getValue()));

    }

    private void editPassenger(Passenger passenger) {
        if (passenger != null) {
            form.setPassenger(passenger);
            form.setVisible(true);
        }

    }

    private void updateDataGrid() {
        //grid.setItems(passengerService.findAll());
        grid.setItems(passengerService.findAll(connection));
    }

    private void updateDataGridByAge() {
        grid.setItems(passengerService.findAll(connection, Integer.parseInt(filterTextAge.getValue())));
    }


    private void savePassenger(PassengerForm.SaveEvent event)  {
        System.out.println("this is save event");
        try {
            passengerService.insert(connection, event.getPassenger());
        } catch (SQLException sqlException){
            System.out.println(sqlException);
        }
        updateDataGrid();
    }

    private void deletePassenger(PassengerForm.DeleteEvent event){
        System.out.println("this is delete event");
        //passengerService.delete(event.getPassenger().getPassengerId());
        updateDataGrid();
    }

    private void updatePassenger(PassengerForm.UpdateEvent event){
        System.out.println("this is update event");
        try {
            passengerService.update(connection, event.getPassenger());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        updateDataGrid();
    }

    private void deletePassengersByAge(){
        System.out.println("delete by name" + deleteAgesField.getValue());
        passengerService.deleteByName(connection, deleteAgesField.getValue());
    }
}
