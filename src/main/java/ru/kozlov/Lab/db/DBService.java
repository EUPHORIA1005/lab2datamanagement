package ru.kozlov.Lab.db;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.*;

@Service
@AllArgsConstructor
public class DBService {

    public Connection connectToDb(String dbname, String user, String pass){
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + dbname, user, pass);
            if (conn != null){
                System.out.println("Connection established");
            }
            else{
                System.out.println("Connection failed");
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
        return conn;
    }

    // Create Database

    public void dropDBIfExists(Connection connection, String dbName) throws SQLException {
        Statement statement = null;
        try {
            String query = "DROP DATABASE IF EXISTS " + dbName + " WITH (FORCE);";
            statement = connection.createStatement();
            statement.executeQuery(query);
        }
        catch (SQLException e){
            System.out.println(e);
        }
        finally {
            statement.close();
        }
    }
    public void createDatabase(Connection connection, String dbName) throws SQLException {
        Statement statement = null;
        try {
            String query = "CREATE DATABASE " + dbName + ";";
            statement = connection.createStatement();
            statement.executeUpdate(query);
        }
        catch (SQLException e){
            System.out.println(e);
        }
    }

    public void createUserRO(Connection connection, String username, String password){
        String query = "CREATE USER " + username +" IDENTIFIED BY " + password;
        CallableStatement statement;
        try{
            statement = connection.prepareCall(query);
            //statement.setString(1, username);
            //statement.setString(2, password);
            statement.execute();

            /*String grantQuery = "GRANT SELECT ON *.* TO ?@'localhost'";
            PreparedStatement grantStatement = connection.prepareStatement(grantQuery);
            grantStatement.setString(1, username);
            grantStatement.execute();*/


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void createTable(Connection connection, String tableName) throws SQLException {
        CallableStatement statement;
        try {
            statement = connection.prepareCall("SELECT createTable(?)");
            statement.setString(1, tableName);
            statement.executeQuery();
        }
        catch (SQLException exception){
            System.out.println(exception);
        }
    }

    public void createFindAll(Connection connection, String tableName) throws SQLException {
        Statement statement = connection.createStatement();
        try{
            statement.executeUpdate("CREATE OR REPLACE FUNCTION findAll(tableName varchar(30))\r\n" +
                    "RETURNS SETOF " +  tableName + "\r\n" +
                    "AS\r\n" +
                    "$$\r\n" +
                    "DECLARE res record;\r\n" +
                    "BEGIN\r\n" +
                    "FOR res in\r\n" +
                    "EXECUTE 'SELECT * FROM ' ||tableName|| '' loop\r\n" +
                    "return next res;\r\n" +
                    "end loop;\r\n" +
                    "END;\r\n" +
                    "$$ LANGUAGE plpgsql;");

        }
        catch (SQLException sqlException){
            System.out.println(sqlException);
        };
    }

    public void createFucntions(Connection connection, String tableName) throws Exception {
        Statement statement = connection.createStatement();
        try{
            statement.executeUpdate("CREATE OR REPLACE FUNCTION findAll(tab_name varchar(30))\r\n" +
                    "RETURNS SETOF " +  tableName + "\r\n" +
                    "AS\r\n" +
                    "$$\r\n" +
                    "DECLARE res record;\r\n" +
                    "BEGIN\r\n" +
                    "FOR res in\r\n" +
                    "EXECUTE 'SELECT * FROM ' ||tab_name|| '' loop\r\n" +
                    "return next res;\r\n" +
                    "end loop;\r\n" +
                    "END;\r\n" +
                    "$$ LANGUAGE plpgsql;");

        }
        catch (SQLException sqlException){
            System.out.println(sqlException);
        };
        try{
        statement.executeUpdate("CREATE OR REPLACE FUNCTION createTable(tableName varchar(30))\r\n" +
                "RETURNS void\r\n" +
                "AS\r\n" +
                "$$\r\n" +
                "BEGIN\r\n" +
                "EXECUTE 'CREATE TABLE IF NOT EXISTS '|| tableName ||'(\r\n" +
                "id int PRIMARY KEY CHECK (id > 0),\r\n" +
                "name text NOT NULL,\r\n" +
                "age int NOT NULL \r\n" +
                ")';\r\n" +
                "END;\r\n" +
                "$$ LANGUAGE plpgsql;");
        }
		catch (SQLException sqlException) {};
		try {
        statement.executeUpdate("CREATE OR REPLACE FUNCTION clearTable(tableName varchar(30))\r\n" +
                "RETURNS void\r\n" +
                "AS\r\n" +
                "$$\r\n" +
                "BEGIN\r\n" +
                "EXECUTE 'TRUNCATE '|| tableName ||'';\r\n" +
                "END;\r\n" +
                "$$ LANGUAGE plpgsql;");
    }
		catch (SQLException sqlException) {};
		try {
            System.out.println("insert function!");
        statement.executeUpdate("CREATE OR REPLACE FUNCTION insertTable(id int, name VARCHAR(30), age int)\r\n" +
                "RETURNS void\r\n" +
                "AS\r\n" +
                "$$\r\n" +
                "BEGIN\r\n" +
                "INSERT INTO " + tableName + " (id, name, age)\r\n" +
                "VALUES (id, name, age\r\n" +
                ");\r\n" +
                "END;\r\n" +
                "$$ LANGUAGE plpgsql;");

        }
		catch (SQLException sqlException) {};
		try {
        statement.executeUpdate("CREATE OR REPLACE FUNCTION findByAge(tableName varchar(30), age_val int)\r\n" +
                "RETURNS SETOF " +  tableName + "\r\n" +
                "AS\r\n" +
                "$$\r\n" +
                "DECLARE res record;\r\n" +
                "BEGIN\r\n" +
                "FOR res in\r\n" +
                "EXECUTE 'SELECT * FROM ' ||tableName|| ' WHERE '||age_val||' = age' loop\r\n" +
                "return next res;\r\n" +
                "end loop;\r\n" +
                "END;\r\n" +
                "$$ LANGUAGE plpgsql;");

        }
		catch (SQLException sqlException) {};
		try {
        statement.executeUpdate("CREATE OR REPLACE FUNCTION updateTable(up_id int, up_name VARCHAR(50)," +
                " up_age int )\r\n" +
                "RETURNS VOID \r\n" +
                "AS\r\n" +
                "$$\r\n" +
                "BEGIN\r\n" +
                "UPDATE " + tableName + " SET name = up_name,  age = up_age\r\n" +
                "WHERE up_id = id ;\r\n" +
                "END;\r\n" +
                "$$ LANGUAGE plpgsql;");

        }
		catch (SQLException sqlException) {};
		try {
        statement.executeUpdate("CREATE OR REPLACE FUNCTION deleteByName(tableName varchar(30), name_val varchar(30))\r\n" +
                "RETURNS void\r\n" +
                "AS\r\n" +
                "$$\r\n" +
                "BEGIN\r\n" +
                "DELETE FROM " + tableName + " WHERE name = name_val;\r\n" +
                "END;\r\n" +
                "$$ LANGUAGE plpgsql;");
    }
		catch (SQLException sqlException) {}
        finally {
            statement.close();
        }
    }
}
