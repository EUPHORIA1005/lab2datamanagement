package ru.kozlov.Lab.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Passenger {

    int passengerId;
    String name;
    int age;

    public String[] toStringArray() {
        String[] passenger = {
                String.valueOf(passengerId),
                name,
                String.valueOf(age)
        };
        return passenger;
    }

    public String toString() {
        return new String(String.valueOf(passengerId) + "," +
                "\"" + name + "\"" + "," +
                String.valueOf(age)
                );
    }

}
